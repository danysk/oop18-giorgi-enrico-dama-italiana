package application;
import javax.swing.*;
import controller.ControllerImpl;

/**
 * playDama start the application
  */
public class PlayDama extends JFrame
{

    private static final long serialVersionUID = -3152739538661070117L;

    /**
     *  start the game
     */
    public static void main(String[] args) 
    {

        ControllerImpl c = new ControllerImpl();
        c.startGame();

    }

}
